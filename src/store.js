import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    messages: [
      {
        id: 1,
        username: 'jacobschatz1',
        msg: 'Welcome to your VueX App! With Vue.js!',
      },
      {
        id: 2,
        username: 'jeff',
        msg: 'Thanks a lot Jacob!',
      },
    ],
  },
  getters: {
    numOfMessages(state) {
      return state.messages.length;
    },
  },
  mutations: {
    addMessage(state, message) {
      state.messages.push({
        id: state.messages.length + 1,
        username: 'jacobschatz1',
        msg: message,
      });
    },
  },
  actions: {
    submitMessage({ commit }, message) {
      commit('addMessage', message);
    },
  },
});
